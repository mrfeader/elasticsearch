﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Services
{
    public abstract class Base<TEntity> where TEntity:class
    {
        protected DbContext DbContext;
        protected DbSet<TEntity> EntityDbSet { get; }

        public Base(DbContext dbContext)
        {
            DbContext = dbContext ?? throw new ArgumentNullException($"{nameof(dbContext)} is null");
            EntityDbSet = dbContext.Set<TEntity>();
        }
        public virtual async Task<ServiceResult> CreateAsync(TEntity entity)
        {
            try
            {
                await EntityDbSet.AddAsync(entity).ConfigureAwait(false);
                await DbContext.SaveChangesAsync().ConfigureAwait(false);
            }
            catch (Exception e)
            {
                return new ServiceResult(false, e);
            }

            return new ServiceResult(true);
        }
        public virtual async Task<ServiceResult> UpdateAsync(int id, TEntity entity)
        {
            try
            {
                var updateEntity = await DbContext.FindAsync<TEntity>(id).ConfigureAwait(false);
                if (updateEntity == null)
                {
                    throw new Exception("entity not found");
                }

                DbContext.Entry(updateEntity).State = EntityState.Detached;
                EntityDbSet.Update(entity);
                await DbContext.SaveChangesAsync().ConfigureAwait(false);
            }
            catch (Exception e)
            {
                return new ServiceResult(false, e);
            }

            return new ServiceResult(true);
        }
        public virtual async Task<ServiceResult> DeleteAsync(TEntity entity)
        {
            try
            {
                EntityDbSet.Remove(entity);
                await DbContext.SaveChangesAsync().ConfigureAwait(false);
            }
            catch (Exception e)
            {
                return new ServiceResult(false, e);
            }

            return new ServiceResult(true);
        }
    }
}
