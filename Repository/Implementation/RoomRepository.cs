﻿using Microsoft.EntityFrameworkCore;
using Repository.interfaces;
using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Services
{
    public class RoomRepository : Base<Room>, IRoomRepository
    {
        public RoomRepository(EFDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<Room> GetByIdOrDefaultAsync(int id)
        {
            try
            {
                return await EntityDbSet.SingleOrDefaultAsync(el => el.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<Room>> GetListAsync()
        {
            try
            {
                return await EntityDbSet.ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<Room>> GetByPredicateAsync(bool asc, Func<Room, bool> predicate = null, string includeProperties = "", string orderBy = "")
        {
            IQueryable<Room> query = EntityDbSet;
            if (predicate != null)
            {
                Expression<Func<Room, bool>> filter = employee => predicate(employee);
                query = query.Where(filter);
            }

            query = QueryConstructor(query, asc, includeProperties, orderBy);
            try
            {
                return await query.ToListAsync().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private IQueryable<Room> QueryConstructor(IQueryable<Room> query, bool asc, string includeProperties = "", string orderBy = "")
        {
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return query.AsNoTracking();
        }
    }
}
