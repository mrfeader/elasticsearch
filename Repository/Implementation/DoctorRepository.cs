﻿using Microsoft.EntityFrameworkCore;
using Repository.interfaces;
using Repository.Models;
using Repository.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Repository
{
    public class DoctorRepository : Base<Doctor>, IDoctorRepository
    {
        public DoctorRepository(EFDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<Doctor> GetByIdOrDefaultAsync(int id)
        {
            try
            {
                return await EntityDbSet.SingleOrDefaultAsync(el => el.ID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<Doctor>> GetListAsync()
        {
            try
            {
                return await EntityDbSet.ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<Doctor>> GetByPredicateAsync(bool asc, Func<Doctor, bool> predicate = null, string includeProperties = "", string orderBy = "")
        {
            IQueryable<Doctor> query = EntityDbSet;
            if (predicate != null)
            {
                Expression<Func<Doctor, bool>> filter = employee => predicate(employee);
                query = query.Where(filter);
            }

            query = QueryConstructor(query, asc, includeProperties, orderBy);
            try
            {
                return await query.ToListAsync().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private IQueryable<Doctor> QueryConstructor(IQueryable<Doctor> query, bool asc, string includeProperties = "", string orderBy = "")
        {
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return query.AsNoTracking();
        }
    }
}
