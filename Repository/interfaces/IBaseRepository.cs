﻿using Repository.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.interfaces
{
    public interface IBaseRepository<TKey, TEntity>
    {
        Task<ServiceResult> CreateAsync(TEntity entity);

        Task<ServiceResult> DeleteAsync(TEntity entity);

        Task<ServiceResult> UpdateAsync(TKey id, TEntity entity);
    }
}
