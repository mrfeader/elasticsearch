﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repository.interfaces
{
    public interface IDoctorRepository: IBaseRepository<int, Doctor> 
    {
        public Task<Doctor> GetByIdOrDefaultAsync(int id);
        public Task<List<Doctor>> GetListAsync();
        public Task<IEnumerable<Doctor>> GetByPredicateAsync(bool asc, Func<Doctor, bool> predicate = null, string includeProperties = "", string orderBy = "");
    }
}
