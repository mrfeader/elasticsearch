﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repository.interfaces
{
    public interface IRoomRepository : IBaseRepository<int, Room>
    {
        public Task<Room> GetByIdOrDefaultAsync(int id);
        public Task<List<Room>> GetListAsync();
        public Task<IEnumerable<Room>> GetByPredicateAsync(bool asc, Func<Room, bool> predicate = null, string includeProperties = "", string orderBy = "");
    }
}
