﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repository.interfaces
{
    public interface IEquipmentRepository: IBaseRepository<int, Equipment>
    {
        public Task<Equipment> GetByIdOrDefaultAsync(int id);
        public Task<List<Equipment>> GetListAsync();
        public Task<IEnumerable<Equipment>> GetByPredicateAsync(bool asc, Func<Equipment, bool> predicate = null, string includeProperties = "", string orderBy = "");
    }
}
