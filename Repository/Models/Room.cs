﻿using System.Collections.Generic;

namespace Repository.Models
{
    public class Room : BaseEntity
    {
        public int Number { get; set; }
        public List<Equipment> Equipment { get; set; }
    }
}
