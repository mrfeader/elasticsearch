﻿namespace Repository.Models
{
    public interface IBaseEntity
    {
        int ID{ get; set; }
    }
}
