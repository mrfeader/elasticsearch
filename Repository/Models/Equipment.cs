﻿namespace Repository.Models
{
    public class Equipment : BaseEntity
    {
        public string Name { get; set; }
        public string Specialization { get; set; }
        public int RoomId { get; set; }
        public Room Room { get; set; }
    }
}
