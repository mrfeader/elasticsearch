﻿using System;

namespace Repository.Services
{
    public class ServiceResult
    {
        public bool OK { get; set; }
        public Exception Exception { get; set; }
        public ServiceResult(bool ok)
        {
            OK = ok;
        }
        public ServiceResult(bool ok, Exception ex)
        {
            OK = ok;
            Exception = ex;
        }
    }
}