﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Models
{
    public class BaseEntity : IBaseEntity
    {
        public int ID { get; set; }
    }
}
