﻿using AutoMapper;
using Repository.interfaces;
using Repository.Models;
using Service.interfaces;
using Service.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.implementation
{
    public class EquipmentService : IEquipmentService
    {
        private readonly IEquipmentRepository _equipmentRepository;
        private readonly IMapper _mapper;

        public EquipmentService(IEquipmentRepository equipmentRepository, IMapper mapper)
        {
            _mapper = mapper;
            _equipmentRepository = equipmentRepository;
        }
        public async Task<EquipmentBll> GetById(int id)
        {
            return _mapper.Map<EquipmentBll>(await _equipmentRepository.GetByIdOrDefaultAsync(id));
        }
        public async Task<IEnumerable<EquipmentBll>> GetAll()
        {
            return _mapper.Map<IEnumerable<EquipmentBll>>(await _equipmentRepository.GetListAsync());
        }
        public async Task<EquipmentBll> Create(EquipmentBll entity)
        {
            return _mapper.Map<EquipmentBll>(await _equipmentRepository.CreateAsync(_mapper.Map<Equipment>(entity)));
        }
        public async Task<EquipmentBll> Delete(int id)
        {
            var equipment = await _equipmentRepository.GetByIdOrDefaultAsync(id);
            return _mapper.Map<EquipmentBll>(await _equipmentRepository.DeleteAsync(equipment));
        }
        public async Task<EquipmentBll> Update(EquipmentBll entity)
        {
            return _mapper.Map<EquipmentBll>(await _equipmentRepository.UpdateAsync(entity.Id, _mapper.Map<Equipment>(entity)));
        }
    }
}
