﻿using AutoMapper;
using Repository.interfaces;
using Repository.Models;
using Service.interfaces;
using Service.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.implementation
{
    public class DoctorService : IDoctorService
    {
        private readonly IDoctorRepository _doctorRepository;
        private readonly IMapper _mapper;
        public DoctorService(IDoctorRepository doctorRepository, IMapper mapper)
        {
            _doctorRepository = doctorRepository;
            _mapper = mapper;
        }
        public async Task<DoctorBll> GetById(int id)
        {
            return _mapper.Map<DoctorBll>(await _doctorRepository.GetByIdOrDefaultAsync(id));
        }
        public async Task<IEnumerable<DoctorBll>> GetAll()
        {
            return _mapper.Map<IEnumerable<DoctorBll>>(await _doctorRepository.GetListAsync());
        }
        public async Task<DoctorBll> Create(DoctorBll entity)
        {
            return _mapper.Map<DoctorBll>(await _doctorRepository.CreateAsync(_mapper.Map<Doctor>(entity)));
        }
        public async Task<DoctorBll> Delete(int id)
        {
            var doct = await _doctorRepository.GetByIdOrDefaultAsync(id);
            return _mapper.Map<DoctorBll>(await _doctorRepository.DeleteAsync(doct));
        }
        public async Task<DoctorBll> Update(DoctorBll entity)
        {
            return _mapper.Map<DoctorBll>(await _doctorRepository.UpdateAsync(entity.Id, _mapper.Map<Doctor>(entity)));
        }
    }
}
