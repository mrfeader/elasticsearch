﻿using AutoMapper;
using Repository.interfaces;
using Repository.Models;
using Service.interfaces;
using Service.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.implementation
{
    public class RoomService : IRoomService
    {
        private readonly IRoomRepository _roomRepository;
        private readonly IMapper _mapper;

        public RoomService(IRoomRepository roomRepository, IMapper mapper)
        {
            _mapper = mapper;
            _roomRepository = roomRepository;
        }
        public async Task<RoomBll> GetById(int id)
        {
            return _mapper.Map<RoomBll>(await _roomRepository.GetByIdOrDefaultAsync(id));
        }
        public async Task<IEnumerable<RoomBll>> GetAll()
        {
            return _mapper.Map<IEnumerable<RoomBll>>(await _roomRepository.GetListAsync());
        }
        public async Task<RoomBll> Create(RoomBll entity)
        {
            return _mapper.Map<RoomBll>(await _roomRepository.CreateAsync(_mapper.Map<Room>(entity)));
        }
        public async Task<RoomBll> Delete(int id)
        {
            var room = await _roomRepository.GetByIdOrDefaultAsync(id);
            return _mapper.Map<RoomBll>(await _roomRepository.DeleteAsync(room));
        }
        public async Task<RoomBll> Update(RoomBll entity)
        {
            return _mapper.Map<RoomBll>(await _roomRepository.UpdateAsync(entity.Id, _mapper.Map<Room>(entity)));
        }
    }
}
