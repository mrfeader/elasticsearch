﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Models
{
    public class DoctorBll
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public int RoomId { get; set; }
        public int RoomNumber { get; set; }
        public string Specialization { get; set; }
    }
}
