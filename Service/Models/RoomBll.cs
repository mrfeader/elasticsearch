﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Models
{
    public class RoomBll
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public List<EquipmentBll> Equipment{ get; set; }
    }
}
