﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Models
{
    public class EquipmentBll
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public string Specialization { get; set; }
        public string Name { get; set; }
    }
}
