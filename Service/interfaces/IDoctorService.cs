﻿using Repository.interfaces;
using Service.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.interfaces
{
    public interface IDoctorService
    {
        Task<DoctorBll> GetById(int id);
        Task<IEnumerable<DoctorBll>> GetAll();
        Task<DoctorBll> Create(DoctorBll entity);
        Task<DoctorBll> Update(DoctorBll entity);
        Task<DoctorBll> Delete(int id);
    }
}
