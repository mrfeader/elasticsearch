﻿using Service.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.interfaces
{
    public interface IEquipmentService
    {
        Task<EquipmentBll> GetById(int id);
        Task<IEnumerable<EquipmentBll>> GetAll();
        Task<EquipmentBll> Create(EquipmentBll entity);
        Task<EquipmentBll> Update(EquipmentBll entity);
        Task<EquipmentBll> Delete(int entity);
    }
}
