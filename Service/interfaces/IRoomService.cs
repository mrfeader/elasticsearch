﻿using Service.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.interfaces
{
    public interface IRoomService
    {
        Task<RoomBll> GetById(int id);
        Task<IEnumerable<RoomBll>> GetAll();
        Task<RoomBll> Create(RoomBll entity);
        Task<RoomBll> Update(RoomBll entity);
        Task<RoomBll> Delete(int entity);
    }
}
